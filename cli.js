#!/usr/bin/env node
const fs = require('fs')
const yargs = require('yargs')
const debug = require('debug')('cobox-hub-cli')
const findUp = require('find-up')
const constants = require('cobox-constants')
const config = require('@coboxcoop/config')
const YAML = require('js-yaml')

const args = yargs
  .config(config.load())
  .commandDir('bin')
  .demandCommand()
  .alias('h', 'help')
  .alias('v', 'version')
  .help()

if (require.main === module) return args
  .usage('Usage: $0 <command> [options]')
  .argv

module.exports = args
