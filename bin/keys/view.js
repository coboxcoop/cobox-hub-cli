const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const options = require('../lib/options')

exports.command = 'view'
exports.desc = "view your hub's public key"
exports.builder = pick(options, ['port'])
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    var data = await client.get('profile')
    process.stdout.write(JSON.stringify(data))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}
