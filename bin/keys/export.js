const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const pick = require('lodash.pick')
const options = require('../lib/options')

exports.command = 'export'
exports.desc = "export your hub's secret key"
exports.builder = pick(options, ['port'])
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    var data = await client.get(['keys'])
    process.stdout.write(JSON.stringify(data))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}
