exports.command = "broadcast <command>"
exports.desc = "announce authentication details over the local area network"
exports.builder = function (yargs) {
  return yargs
    .commandDir('broadcast')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
