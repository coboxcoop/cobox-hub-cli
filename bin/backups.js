exports.command = "backups <command>"
exports.desc = "back-up your peers' spaces"
exports.builder = function (yargs) {
  return yargs
    .commandDir('backups')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
