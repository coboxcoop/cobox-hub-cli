const chalk = require('chalk')
const DEFAULT_COLOUR = '#ACFFAC'

function onSuccess (msgs) {
  msgs = Array.isArray(msgs) ? msgs : [msgs]
  for (const msg of msgs) console.log(chalk.hex(DEFAULT_COLOUR)(msg, '\n'))
}

function onError (errs) {
  errs = Array.isArray(errs) ? errs : [errs]
  for (const err of errs) console.error(chalk.red(err), '\n')
}

module.exports = {
  DEFAULT_COLOUR,
  onSuccess,
  onError
}
