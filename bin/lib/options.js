module.exports = {
  udp: {
    alias: 'u',
    number: true,
    describe: 'udp packet port',
    default: 8999
  },
  port: {
    alias: 'p',
    number: true,
    describe: 'hub server port',
    default: 9111
  },
  address: {
    description: 'address of the space',
    type: 'string',
    alias: 'a',
  },
  name: {
    description: 'name of the space',
    type: 'string',
    alias: 'n'
  },
  code: {
    description: 'an invite code',
    type: 'string',
    alias: 'c',
  }
}
