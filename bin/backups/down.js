const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const chalk = require('chalk')
const pick = require('lodash.pick')
const options = require('../lib/options')

exports.command = 'down [options]'
exports.description = 'stop all current backups, or remove an existing backup'
exports.builder = pick(options, ['name', 'address', 'port'])
exports.handler = async function up (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let replicators = await client.get('replicators')

    if (!name && !address) {
      if (!replicators.length) return console.log(`! ${chalk.bold(`you have no existing backups`)}, create one with\n${chalk.hex('#ACFFAC')(`hub backups up --name <name> --address <address>`)}`)
      await client.delete(['replicators', 'connections'])
      process.stdout.write(JSON.stringify(replicators))
    } else {
      let replicator = replicators.find((replicator) => (
        replicator.name === name || replicator.address === address)
      )

      if (replicator) {
        let data = await client.delete(['replicators', replicator.address, 'connections'], {}, replicator)
        process.stdout.write(JSON.stringify(data))
      } else {
        return console.log(`! ${chalk.bold(`backup doesn't exist`)}`)
      }
    }
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}
