const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const options = require('../lib/options')

exports.command = 'ls'
exports.desc = "list your hub's backups"
exports.builder = { port: options.port }
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let replicators = await client.get('replicators')
    process.stdout.write(JSON.stringify(replicators))
  } catch (err) {
    process.stderr.write(err)
  }
}
