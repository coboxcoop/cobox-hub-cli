const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const chalk = require('chalk')
const options = require('../lib/options')
const pick = require('lodash.pick')

exports.command = 'stop'
exports.description = 'stop broadcasting'
exports.builder = pick(options, ['port'])
exports.handler = async function stop (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let data = await client.delete(['commands', 'announcements'])
    process.stdout.write(JSON.stringify(data))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}
