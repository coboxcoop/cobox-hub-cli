const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const chalk = require('chalk')
const pick = require('lodash.pick')
const options = require('../lib/options')

exports.command = 'start [options]'
exports.description = 'start broadcasting'
exports.builder = pick(options, ['port', 'udp'])
exports.handler = async function start (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let data = await client.post(['commands', 'announcements'])
    process.stdout.write(JSON.stringify(data))
  } catch (err) {
    if (!err.response) return console.error(err)
    var errors = err.response.data.errors
    errors.forEach((err) => process.stderr.write(err.msg))
  }
}
