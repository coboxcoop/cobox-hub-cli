# cobox-hub-cli

A command line interface for cobox-hub. Start the app, backup your peers, and view your hub's identity and manage settings.
